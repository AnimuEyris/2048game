package my.game.util;

public class CoordinatePoint {

    private Integer x;
    private Integer y;

    public CoordinatePoint(Integer x, Integer y){
        this.x = x;
        this.y = y;
    }

    public CoordinatePoint(int x, int y){
        this.x = x;
        this.y = y;
    }

    public Integer getX(){
        return x;
    }

    public Integer getY(){
        return y;
    }

    public void setX(Integer x){
        this.x = x;
    }

    public void setY(Integer y){
        this.y = y;
    }

    public void setX(int x){
        this.x = x;
    }

    public void setY(int y){
        this.y = y;
    }
}
