package my.game.application;

import my.game.util.CoordinatePoint;

import javax.swing.*;
import java.awt.*;
import java.awt.event.KeyEvent;
import java.awt.event.KeyListener;
import java.awt.event.WindowEvent;
import java.util.ArrayList;
import java.util.Random;

public class Game extends JFrame implements KeyListener {

    private final int windowSize = 500;
    private final int windowBorderGap = 50;
    private final int fieldsNo = 4;
    private final int numberFontSize = 40;
    private final String numberFontName = "Arial";
    private final int[] possibleNewNumbers = {2,4};

    private final int LEFT_KEY  = 37;
    private final int UP_KEY    = 38;
    private final int RIGHT_KEY = 39;
    private final int DOWN_KEY  = 40;

    private final String gameOverMessage = "Game Over!";


    private Integer[][] board={
            {0,2,0,0},
            {0,2,0,0},
            {0,0,0,0},
            {0,0,0,0}
    };

    private int newTileRow=0;
    private int newTileColumn=0;

    public Game(){
        setSize(windowSize,windowSize);
        setDefaultCloseOperation(WindowConstants.EXIT_ON_CLOSE);
    }


    public void run(){
        addKeyListener(this);
        setVisible(true);
    }

    @Override
    public void paint(Graphics g) {
        Image image = createImage(windowSize, windowSize);
        Graphics2D graphics2D = (Graphics2D) image.getGraphics();

        graphics2D.setColor(Color.LIGHT_GRAY);
        graphics2D.fillRect(windowBorderGap, windowBorderGap, windowSize-2*windowBorderGap, windowSize-2*windowBorderGap);
        for(int row=0; row<fieldsNo; row++){
            for(int column=0; column<fieldsNo; column++){

                if( newTileRow==row && newTileColumn==column && board[newTileRow][newTileColumn]>0){
                    graphics2D.setColor(Color.gray);
                    graphics2D.fillRect(windowBorderGap+newTileColumn*100, windowBorderGap+newTileRow*100, 100, 100);
                }

                graphics2D.setColor(Color.RED);
                int increment = (windowSize-2*windowBorderGap)/fieldsNo;
                for(int position=windowBorderGap; position<windowSize; position+=increment){
                    graphics2D.drawLine(windowBorderGap, position, windowSize-windowBorderGap, position);
                    graphics2D.drawLine(position, windowBorderGap, position, windowSize-windowBorderGap);
                }

                if(board[row][column]>0) {
                    String numToPrint = board[row][column].toString();
                    graphics2D.setFont(new Font(numberFontName, Font.BOLD, numberFontSize));
                    graphics2D.setColor(Color.BLUE.darker());
                    graphics2D.drawString(numToPrint, 70 + column * 100, 120 + row * 100);
                }

            }
        }

        g.drawImage(image, 0,0, this);
    }

    public void addRandomTile(){
        ArrayList<CoordinatePoint> freeFields = new ArrayList<>();
        Random random = new Random();

        for(int i=0; i<fieldsNo; i++){
            for(int j=0; j<fieldsNo; j++){
                if(board[i][j] == 0){
                    freeFields.add(new CoordinatePoint(i,j));
                }
            }
        }

        if(freeFields.size() == 0){
            JOptionPane.showMessageDialog(this, gameOverMessage);
            dispatchEvent(new WindowEvent(this, WindowEvent.WINDOW_CLOSING));
        } else {
            CoordinatePoint newTilePosition = freeFields.get(random.nextInt(freeFields.size()));
            newTileRow = newTilePosition.getX();
            newTileColumn = newTilePosition.getY();
            board[newTileRow][newTileColumn] = possibleNewNumbers[random.nextInt(possibleNewNumbers.length)];
        }
    }

    private void joinTiles(Integer keyCode){
        boolean isRightOrDown = (keyCode == RIGHT_KEY || keyCode == DOWN_KEY ? true : false);

        for(int row=0; row<fieldsNo; row++) {
            for(int col=0; col<fieldsNo; col++) {
                switch(keyCode) {
                    case RIGHT_KEY: joinRight(fieldsNo-1-row , fieldsNo-1-col); continue;
                    case DOWN_KEY:  joinDown(fieldsNo-1-row, fieldsNo-1-col);  continue;
                    case LEFT_KEY:  joinLeft(row, col);  continue;
                    case UP_KEY:    joinUp(row, col);    continue;
                    default: return;
                }
            }
        }

    }

    private void joinDown(int x, int y)
    {
        int value = board[x][y];
        int ile = 0;

        for(int i=1; x+i<fieldsNo;i++) {
            if(board[x+i][y]==0) {
                board[x+i][y]=value;
                board[x+i-1][y]=0;
            }
            else if(value==board[x+i][y]&&(ile==0)) {
                value = value*2;
                board[x+i][y]=value;
                board[x+i-1][y]=0;
                ile++;
            }
            else {
                break;
            }
        }
    }

    private void joinUp(int x, int y)
    {
        int value = board[x][y];
        int ile = 0;

        for(int i=1; x-i>=0;i++) {
            if(board[x-i][y]==0){
                board[x-i][y]=value;
                board[x-i+1][y]=0;
            }
            else if(value==board[x-i][y]&&(ile==0)) {
                value = value*2;
                board[x-i][y]=value;
                board[x-i+1][y]=0;
                ile++;
            }
            else {
                break;
            }
        }
    }

    private void joinLeft(int x, int y)
    {
        int value = board[x][y];
        int ile=0;

        for(int i=1; y-i>=0;i++) {
            if(board[x][y-i]==0) {
                board[x][y-i]=value;
                board[x][y-i+1]=0;
            }
            else if(value==board[x][y-i]&&(ile==0)) {
                value = value*2;
                board[x][y-i] = value;
                board[x][y-i+1] = 0;
                ile++;
            }
            else {
                break;
            }
        }
    }

    private void joinRight(int x, int y)
    {
        int value = board[x][y];
        int ile = 0;

        for(int i=1; y+i<fieldsNo;i++) {
            if(board[x][y+i]==0) {
                board[x][y+i]=value;
                board[x][y+i-1]=0;
            }
            else if(value==board[x][y+i]&&(ile==0)) {
                value = value*2;
                board[x][y+i] = value;
                board[x][y+i-1] = 0;
                ile++;
            }
            else {
                break;
            }
        }
    }

    @Override
    public void keyTyped(KeyEvent e) {}

    @Override
    public void keyPressed(KeyEvent e) {
        joinTiles(e.getKeyCode());
        addRandomTile();
        repaint();
    }

    @Override
    public void keyReleased(KeyEvent e) {}
}
