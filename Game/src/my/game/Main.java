package my.game;

import my.game.application.Game;

public class Main {

    public static void main(String[] args)
    {
        Game game = new Game();
        game.run();
    }

}
